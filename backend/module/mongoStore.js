var mongodb = require('mongodb'),
    MongoClient = mongodb.MongoClient,
    geoip = require('geoip-lite'),
    url = 'mongodb://localhost:27017/getLog',
    parseUAG = require('user-agent-parser');

module.exports=function (dbNm,req) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);
            var collection = db.collection(dbNm);
            var data = {};
            if(req.body)
            data.body=req.body;
            data.pragma=req.headers.pragma;
            data.userAgent=parseUAG(req.headers["user-agent"]);
            data.serverTime=new Date();
            collection.insert([data], function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Inserted %d documents into the collection. The documents inserted with "_id" are:', result.length);
                }
                db.close();
            });
        }
    });
};