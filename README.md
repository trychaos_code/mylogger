# Node application to log all user activity and errors in some database.

The application provides following features:

 * **httpLog:** It intercepts all/filtered http requests with their fields and log them to mongoDB
 * **hashChng:** It can be called whenever we need to log hash change events.
 * **locChng:** It detects all location changes and log them to DB.
 * **logDta:** It captures all click events on page and log data with target specifier.
 * **onError:** It captures all error on browser and log them to DB with it's trace
 * **scrnShot:** It captures the current screenshot and send it's base64 to DB(it can be used to show the hits on the page elements)
 * **flowStatistic:** It provide a custom method to send data to DB


## Running it

Clone this repository as well as [the server](http://code.edgenetworks.in/akhilesh.kumar/nodelogger.git) for this example.

Then, run `npm install` on this project and run `npm start`(which will run command `cd backend && npm i && node server.js`) to start the app. 

Then just navigate to copy paste following code to corresponding page

    var apendScrpt=function (path) {
        var JS= document.createElement('script');
        JS.src= path;
        document.body.appendChild(JS);
    };
    apendScrpt("http://localhost:8082/plugIn.js");

Start your mongoDB and start your app
 :boom: You are done.
 
## Restore DB Data

1. You can restore **demoDB** using command `mongorestore'

## Project configuration:

It exposes three variables:

 * **skipUrls/onlyUrls**
 
    Assign Urls that should skip/choose for httpLog
    
    e.g. `window.skipUrls="abc|.html|logout|login|signin"`
    
 * **pushFlowDta:** It is a exposed function to custom data push:
    e.g.   
 ```   
     pushFlowDta("hcl_demand_analytics_report",
      {
          "sr_id":"HCL/HCL/2016/621499",
          "last_modified_date":"2016-10-03",
          "last_modified_time":"13:25:58",
          "kenexa_candidateid":"8242042",
          "candidate_status":"Offer Accept (EU)"
      });
  ```    
      
 * **myHashchange:** custom send hash change
 
     e.g. 
     `myHashchange(prevLoc, newLoc);`
     where 
     
     **prevLoc:** location object before hash change 
         
     **newLoc:** location object after hash change

## Author
Akhilesh Kumar<akhilesh.kumar@edgenetworks.in>
