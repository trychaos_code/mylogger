var mongodb = require('mongodb'),
    MongoClient = mongodb.MongoClient,
    geoip = require('geoip-lite'),
    url = 'mongodb://localhost:27017/getLog',
    parseUAG = require('user-agent-parser');

module.exports={
    getEmpDet:function (quer,res) {
        //http://localhost:8082/getEmpDet?typ=userName&txt=khiles
        console.log("quer:",quer);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {
                console.log('Connection established to', url);
                var collection = db.collection("scrnShot");
                var typ='body.myIpJson.'+quer.typ;
                var srchReg={'$regex': new RegExp(".*"+quer.txt+".*")};
                var fnnd={};
                fnnd[""+typ]=srchReg;
                console.log("fnnnd",fnnd);
                var myCursor = collection.find(fnnd, {'body.myIpJson': 1}).sort({_id:-1}).toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                    } else{
                        var dta=result.length?result[0].body.myIpJson:"{}";
                        console.log('Found:', dta);
                        res.send(dta);
                    }
                    db.close();
                });
            }
        });
    },
    getStatic:function (quer,res) {
        //db.getCollection('logDta').aggregate({$group: {_id:{"city":'$body.myIpJson.city',"ip":"$body.myIpJson.ip"}}})
        console.log("quer1:",quer);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {
                console.log('Connection established to', url);
                db.collection("logDta").aggregate(
                    {
                    "$group": {
                        _id: 0,
                        "city": {"$addToSet": '$body.myIpJson.city'},
                        "ip": {"$addToSet": '$body.myIpJson.ip'},
                        "country_name": {"$addToSet": '$body.myIpJson.country_name'},
                        "region_name": {"$addToSet": '$body.myIpJson.region_name'},
                        "time_zone": {"$addToSet": '$body.myIpJson.time_zone'},
                        "browser": {"$addToSet": '$userAgent.browser.name'},
                        "os": {"$addToSet": '$userAgent.os.name'},
                        "cpu": {"$addToSet": '$userAgent.cpu.architecture'},
                        "origin": {"$addToSet": '$body.location.origin'},
                        "pathname": {"$addToSet": '$body.location.pathname'},
                        "hash": {"$addToSet": '$body.location.hash'}
                    }
                }, function(err, result) {
                    if (err) {
                        console.log(err);
                    } else{
                        var DemDta=result[0];
                        delete DemDta["_id"];
                        console.log('Found:',DemDta);
                        res.send(DemDta);
                    }
                    db.close();
                })
            }
        });
    },
    getScrnshot:function (quer,res) {
        //db.getCollection('logDta').aggregate({$group: {_id:{"city":'$body.myIpJson.city',"ip":"$body.myIpJson.ip"}}})
        console.log("quer1:",quer);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {
                console.log('Connection established to', url);
                db.collection("scrnShot").aggregate(
                    {
                        "$group": {
                            "_id": {
                                "origin": "$body.location.origin",
                                "pathname": "$body.location.pathname",
                                "hash": "$body.location.hash"
                            },
                            "names": {
                                "$addToSet": {
                                    "base64": "$body.base64",
                                    "hitWid":"$body.hitWid"
                                }
                            }
                        }
                    }, function(err, result) {
                        if (err) {
                            console.log(err);
                        } else{
                            var DemDta=result;
                            console.log('Found:',DemDta);
                            res.send(DemDta);
                        }
                        db.close();
                    })
            }
        });
    },
    getHttpLg:function (quer,res) {
        //db.getCollection('logDta').aggregate({$group: {_id:{"city":'$body.myIpJson.city',"ip":"$body.myIpJson.ip"}}})
        console.log("quer1:",quer);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            } else {
                console.log('Connection established to', url);
                db.collection("httpLog").aggregate(
                    {
                        "$group": {
                            _id: 0,
                            "city": {"$addToSet": '$body.myIpJson.city'},
                            "ip": {"$addToSet": '$body.myIpJson.ip'},
                            "country_name": {"$addToSet": '$body.myIpJson.country_name'},
                            "region_name": {"$addToSet": '$body.myIpJson.region_name'},
                            "time_zone": {"$addToSet": '$body.myIpJson.time_zone'},
                            "browser": {"$addToSet": '$userAgent.browser.name'},
                            "os": {"$addToSet": '$userAgent.os.name'},
                            "cpu": {"$addToSet": '$userAgent.cpu.architecture'},
                            "origin": {"$addToSet": '$body.location.origin'},
                            "pathname": {"$addToSet": '$body.location.pathname'},
                            "hash": {"$addToSet": '$body.location.hash'},
                            "url":{"$addToSet": '$body.data.absurl'},
                        }
                    }, function(err, result) {
                        if (err) {
                            console.log(err);
                        } else{
                            var DemDta=result[0];
                            delete DemDta["_id"];
                            console.log('Found:',DemDta);
                            res.send(DemDta);
                        }
                        db.close();
                    })
            }
        });
    },
};