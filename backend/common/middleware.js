var bodyParser = require('body-parser'),
	cors = require('cors');

module.exports.middleware = function(app,express) {
	app.use(cors());
	app.use(bodyParser.json({limit: '10mb'}));
	app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
	app.use(express.static(__dirname + '/../../store'));
};
