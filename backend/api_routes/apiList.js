var path=require("path"),
    mong=require("../module/mongoStore"),
    gtMong=require("../module/mongoRetrive");
var createToken=function (user) {
    return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 });
};
module.exports ={
    "get":[
        {
            "url":'/',
            "callback":function(req, res, next){
                console.log("__dirname",__dirname);
                res.send({"Success":true});
            }
        }
    ],
    "post":[
        {
            "url":'/api/login',
            "callback":function(req, res, next){
                res.send({
                    'X-Auth-Token':"data.token",
                    'username':"Akhilesh Kumar",
                    'user_type':"data.result['user_type']",
                    'user_role':"L3",
                    'name':" data.result.name",
                    'email':" data.result.email",
                    'clientname':" data.result.clientname",
                    'menu_access':" data.result.menu_access",
                    'jwt':" data.token",
                    'loggedInAt':""+((new Date()).getTime())
                });
            }
        },
        {
            "url":'/getEmpDet',
            "callback":function(req, res, next){
                gtMong.getEmpDet(req.body,res);
            }
        },
        {
            "url":'/getStatic',
            "callback":function(req, res, next){
                gtMong.getStatic(req.body,res);
            }
        },
        {
            "url":'/getScrnshot',
            "callback":function(req, res, next){
                gtMong.getScrnshot(req.body,res);
            }
        },
        {
            "url":'/getHttpLg',
            "callback":function(req, res, next){
                gtMong.getHttpLg(req.body,res);
            }
        }
    ],
    "put":[
        {
            "url":"/akhilLog/getLog",
            "callback":function(req, res, next){
                mong("logDta",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/getError",
            "callback":function(req, res, next){
                mong("onError",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/getHashChang",
            "callback":function(req, res, next){
                mong("hashChng",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/getLocChang",
            "callback":function(req, res, next){
                mong("locChng",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/getScrnShot",
            "callback":function(req, res, next){
                mong("scrnShot",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/getHttpLog",
            "callback":function(req, res, next){
                mong("httpLog",req);
                res.send({"Success":true});
            }
        },
        {
            "url":"/akhilLog/flowStatistic",
            "callback":function(req, res, next){
                mong("flowStatistic",req);
                res.send({"Success":true});
            }
        }
    ]
};


