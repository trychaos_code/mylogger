var processUtil= require("./common/processUtil");

var express = require('express'),
  app = express();
var addApiList= require("././api_routes/apiEngine"),
  middleParse = require('./common/middleware.js');
var ngrok = require('ngrok'),
    fs=require("fs"),
    path=require("path");

middleParse.middleware(app,express);
processUtil(express,app);
addApiList(app);
var srvr=app.listen(8082, function() {
  console.log('listening on port : ' + 8082);
});

var tunnel = ngrok.connect({proto:"http",addr: 8082},function(err, url) {
    if (err){console.log("err: ",err)}
    console.log(url);
    var filPth=path.resolve(__dirname,"../store/plugIn.js");
    console.log("filPth:",filPth);
    fs.readFile(filPth,'utf8', function(err, data1) {
        if (err){
            //throw err;
            console.log(filPth,"read err: ",err);
        }else {
            var replcStr='//myReplceSec567\n\tvar myNgRockUrl="'+url+'";\n\t//myReplceSec568';
            var preStr=data1.split("//myReplceSec567")[0];
            var postStr=data1.split("//myReplceSec568")[1];
            var data=preStr+replcStr+postStr;
            fs.writeFile(filPth, data, function(err) {
                if (err){
                    console.log(filPth,"write err: ",err);
                    //throw err;
                }else {
                    console.log('writeFile is Done');
                }
            });
        }
    });
    var filPth1=path.resolve(__dirname,"../store/code.js");
    console.log("filPth1:",filPth1);
    fs.readFile(filPth1,'utf8', function(err, data1) {
        if (err){
            //throw err;
            console.log(filPth,"read err: ",err);
        }else {
            var replcStr='//myReplceSec567\n\tvar myNgRockUrl="'+url+'";\n\t//myReplceSec568';
            var preStr=data1.split("//myReplceSec567")[0];
            var postStr=data1.split("//myReplceSec568")[1];
            var data=preStr+replcStr+postStr;
            fs.writeFile(filPth1, data, function(err) {
                if (err){
                    console.log(filPth1,"write err: ",err);
                    //throw err;
                }else {
                    console.log('writeFile1 is Done');
                }
            });
        }
    });
});
